App = {
    web3Provider: null,
    contracts: {},
    account: '0x0',
  
    init: function() {
      return App.initWeb3();
    },
    initWeb3: function() {
        if (typeof web3 !== 'undefined') {
          // If a web3 instance is already provided by Meta Mask.
          App.web3Provider = web3.currentProvider;
          web3 = new Web3(web3.currentProvider);
        } else {
          // Specify default instance if no web3 instance provided
          App.web3Provider = new Web3.providers.HttpProvider('http://localhost:7545');
          web3 = new Web3(App.web3Provider);
        }
        return App.initContract();
      },
      initContract: async () => {
        // Create a JavaScript version of the smart contract
        const todoList = await $.getJSON('TodoList.json')
        App.contracts.TodoList = TruffleContract(todoList)
        App.contracts.TodoList.setProvider(App.web3Provider)
    
        // Hydrate the smart contract with values from the blockchain
        // App.todoList = await App.contracts.TodoList.deployed()
        return App.loadContract();
      },
      loadContract: async () => {
        // Create a JavaScript version of the smart contract
        const todoList = await $.getJSON('TodoList.json')
        App.contracts.TodoList = TruffleContract(todoList)
        App.contracts.TodoList.setProvider(App.web3Provider)
    
        // Hydrate the smart contract with values from the blockchain
        App.todoList = await App.contracts.TodoList.deployed()
        return App.render();
      },
      
      render: async () => {
        // Prevent double render
        if (App.loading) {
          return
        }
          // Load account data
        web3.eth.getCoinbase(function(err, account) {
        if (err === null) {
          App.account = web3.eth.accounts[0];
        //   App.account = account;
          console.log("Id de votre compte : " + account);
          $("#account").html("Id de votre compte : " + account);
        }
      });
    
        // Update app loading state
        App.setLoading(true)
    
        // Render Account
        // $('#account').html(App.account)
    
        // Render Tasks
        await App.renderTasks()
    
        // Update loading state
         App.setLoading(false)
      },


      renderTasks: async () => {
        // Load the total task count from the blockchain
        console.log("dans rendertask")
        const taskCount = await App.todoList.taskCount()
        const $taskTemplate = $('.taskTemplate')
        console.log(taskCount)
        // Render out each task with a new task template
        for (var i = 11; i <= (taskCount); i++) {
            console.log(i)
          // Fetch the task data from the blockchain
          const task = await App.todoList.tasks(i)
          const taskId = task[0].toNumber()
          const taskContent = task[1]
          const taskCompleted = task[2]
            console.log("taskContent")
            console.log(taskContent)
          // Create the html for the task
          const $newTaskTemplate = $taskTemplate.clone()
          $newTaskTemplate.find('.content').html(taskContent)
          $newTaskTemplate.find('input')
                          .prop('name', taskId)
                          .prop('checked', taskCompleted)
                          .on('click', App.toggleCompleted)
    
          // Put the task in the correct list
          if (taskCompleted) {
            $('#completedTaskList').append($newTaskTemplate)
          } else {
            $('#taskList').append($newTaskTemplate)
          }
    
          // Show the task
          $newTaskTemplate.show()
        }
        // return App.render();
      },

      createTask: async () => {
        App.setLoading(true)
        const content = $('#newTask').val()
        await App.todoList.createTask(content)
        window.location.reload()
      },
      toggleCompleted: async (e) => {
        App.setLoading(true)
        const taskId = e.target.name
        await App.todoList.toggleCompleted(taskId)
        window.location.reload()
      },
    setLoading: (boolean) => {
        App.loading = boolean
        const loader = $('#loader')
        const content = $('#content')
        if (boolean) {
          loader.show()
          content.hide()
        } else {
          loader.hide()
          content.show()
        }
      }
    }

    $(function() {
        $(window).load(function() {
          App.init();
        });
      });