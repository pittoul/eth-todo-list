# Todo-List

- installer truffle suite (https://trufflesuite.com/) pour avoir `ganache-cli` et Truffle(env de dev pour Solidity, le langage des smart contracts)

# Solidity
Source : https://hackernoon.com/ethereum-development-walkthrough-part-1-smart-contracts-b3979e6e573e
TUTO TODO LIST : https://www.dappuniversity.com/articles/blockchain-app-tutorial



## OUTILS

- npm install -g truffle
- npm install -g ganache-cli

Truffle est env de dev pour Solidity
Ganache est une simulation d'EVM (Etherum Virtual Machine), en adéquation avec les données réseau définies dans Truffle
L'EVM est le moteur de la blockchain d'Etherum. Nos contrats seront déployés dessus, entrant ainsi dans ce flot de données décentralisé/ées (le flot et les données sont décentralisés)

## UTILISATION

### BUILD !
- Créer un projet Truffle. Dans un dossier, faire un : 
        `truffle init`


- Dans le dossier "contracts", créer nos fichiers `.sol` qui sont des classes écrites en langage "Solidity"
- modifier le fichier truffle-config.js pour identifier le réseau
- créer le bon ficheir dans le dossier "migrations" (voir exemples)

### RUN !

Dans notre environnement de Dev :
- Activation de l'EVM et création de 10 comptes disposant chacun de 100ETH:

 `ganache-cli -p 7545`

Dans un autre terminal, création des binaires et envoie dans la blockchain :
 - `truffle compile` pour créer le fichier binaire qui sera exécuté dans l'EVM.
 - Puis `truffle migrate --network nomNetwork` pour déployer le code sur la blockchain 
 - Puis `truffle console --network nomNetwork` pour intéragir avec la blockchain

En console :
todoList = await TodoList.deployed()
task = await todoList.tasks(1)




# Le Langage "Solidity" :
### Les VARIABLES et leur PORTÉE :

#### PORTÉE :
- PUBLIC 

exemple : `address public monAdresse1;`
Donne acces à un guetteur ! Le setter est explicite : il faut l'écrire !

- PRIVATE

Accessible uniquement à l'intérieur du contrat
Attention, cette portée ne permet pas l'héritage !

- INTERNAL 

Comme private, mais permet l'héritage !

#### TYPAGE :

- event : des logs qui peuvent aussi appeler des Callbacks js dans le front

#### LA METHODE require() :

Cette methode permet de lever une exception si les conditions ne sont pas remplies.
Telle une transaction SQL, cette levée d'erreur ramène le système à son état initial.

#### payable :

`function maFonction() public payable {...}`

Le mot clé 'payable' indique que cette méthode accepte la monnaie, ici, de l'ether...




